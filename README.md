# About
This is a boilerplate site for WordPress containing:

- Theme using tailwind css, build scripts, optimized for oxygen
- Plugin for designlibrary, sr-landingpage, for oxygen builder using a collection of tailwind components (not done)
- 

The purpose of this theme is to provide with a quick way of doing new sites, really, really quick.

In a near future, this will probably also be bedrock so its easier to manage plugins, they shouldnt be in repo and i have a feeling there will be many plugins.


## More info
The tailwind css is now custom built for ALL components making it about 4mb in size. This needs to be optimized for customer but is not built yet. There are multiple way to do this but most efficient
is probably to extend on oxygen builders css rewrite. Oxygens css rewrite is per page, so the tailwind components used on a page could be 1-1 match and really, really slim. It doesnt even need tailwind base.

## Installation
- yarn
- yarn dev
